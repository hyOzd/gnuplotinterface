#include <iostream>

#include "gpi.h"

using namespace std;
using namespace gpi;

int main() {

	cout << "gpi example" << endl;

	// generate plot data
	const int N = 20;
	double x[N];
	double y[N];

	for ( int i=0; i<N; i++){
		x[i] = i;
		y[i] = i*i;
	}

	// plot data
	plot(x,y,N);

	return 0;

}

