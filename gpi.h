#ifndef GPI_H_INCLUDED
#define GPI_H_INCLUDED

#include <stdlib.h>
#include <string>
#include <vector>
#include "boost/fdstream.hpp"

namespace gpi {

	using std::endl;

	class context{

		public:

			bool is_pipe_open;
			FILE* gp_pipe;
			boost::fdostream* gpp_stream;

			context(){
				is_pipe_open = false;
			};

			~context(){
				close_gp_pipe();
			};

			void open_gp_pipe(){
				if(!is_pipe_open){
					#ifdef _WIN32
					gp_pipe = popen("gnuplot --persist > /nul 2>&1","w");
					#else
					gp_pipe = popen("gnuplot --persist > /dev/null 2>&1","w");
					#endif
					gpp_stream = new boost::fdostream(fileno(gp_pipe));
					is_pipe_open = true;
				}
			};

			void close_gp_pipe(){
				if(is_pipe_open){
					*gpp_stream<<"exit"<<endl;
					delete gpp_stream;
					pclose(gp_pipe);
					is_pipe_open = false;
				}
			}

	} default_context;

	struct curve {
		std::vector<double> xdata,ydata;
		std::string ctitle;
	};

	std::vector<curve> curve_store;

	void plot_single(double* x, double* y, unsigned int length) {

		*default_context.gpp_stream<<"plot '-' with lines"<<std::endl;

		for(unsigned int i=0; i<length; i++){
			*default_context.gpp_stream<<x[i]<<"\t"<<y[i]<<std::endl;
		}

		*default_context.gpp_stream<<"e"<<std::endl;

	}

	void write_data(std::vector<double> &x, std::vector<double> &y){
		for(unsigned int i=0; i<x.size(); i++){
			*default_context.gpp_stream<<x[i]<<"\t"<<y[i]<<std::endl;
		}
		*default_context.gpp_stream<<"e"<<std::endl;
	}

	void plot_single(std::vector<double> &x, std::vector<double> &y) {

		*default_context.gpp_stream<<"plot '-' with lines"<<std::endl;

		write_data(x,y);

	}

    /** \brief Add plot data for plotting multi-curve.
     * plot() function needs to be called to actually plot data.
     * \param x double*
     * \param y double*
     * \param length unsigned int
     * \param ctitle="" std::string Curve title, will be shown in legend.
     * \return void
     *
     */
	void addplot(double* x, double* y, unsigned int length, std::string ctitle=""){
		curve newcurve;
		newcurve.xdata.resize(length);
		newcurve.ydata.resize(length);
		for(unsigned int i=0; i<length; i++){
			newcurve.xdata[i] = x[i];
			newcurve.ydata[i] = y[i];
		}
		newcurve.ctitle = ctitle;
		curve_store.push_back(newcurve);
	}

    /** \brief Change x-axis label.
     *
     * \param label std::string
     * \return void
     *
     */
	void xlabel(std::string label){
		default_context.open_gp_pipe();
		*default_context.gpp_stream<<"set xlabel '"<<label<<"'"<<endl;
	}

    /** \brief Change y-axis label.
     *
     * \param label std::string
     * \return void
     *
     */
	void ylabel(std::string label){
		default_context.open_gp_pipe();
		*default_context.gpp_stream<<"set ylabel '"<<label<<"'"<<endl;
	}

    /** \brief Change plot title.
     *
     * \param label std::string
     * \return void
     *
     */
	void title(std::string label){
		default_context.open_gp_pipe();
		*default_context.gpp_stream<<"set title '"<<label<<"'"<<endl;
	}

    /** \brief Single curve plot.
     * Simplest way of plotting. Just provide necessary data.
     * If you want to change labels,title etc. Call before this.
     *
     * \param x double*
     * \param y double*
     * \param length unsigned int
     * \return void
     *
     */
	void plot(double* x, double* y, unsigned int length) {

		default_context.open_gp_pipe();	// open gnuplot pipe and stream if not already opened

		plot_single(x,y,length);

		std::cin.get();

		//close_gp();

	}

    /** \brief Multi curve plot.
     * This function doesn't want any parameters because you
     * need to provide data with addplot(...) function. These curves be
     * will plotted together on a single window. Also this
     * function will stop program, and wait for a keystroke from user.
     * \return void
     *
     */
	void plot() {

		if(curve_store.size()>1) {
			default_context.open_gp_pipe();
			*default_context.gpp_stream<<"plot ";

			for(int k=0; k<curve_store.size()-1; k++) {
				*default_context.gpp_stream<<"'-' with lines title '"<<curve_store[k].ctitle<<"', ";
			}

			*default_context.gpp_stream<<"'-' with lines title '"<<curve_store[curve_store.size()-1].ctitle<<"' "<<endl;

			for(int k=0; k<curve_store.size(); k++) {
				write_data(curve_store[k].xdata,curve_store[k].ydata);
			}

		} else if(curve_store.size()==1) {
			default_context.open_gp_pipe();
			title(curve_store[0].ctitle);
			plot_single(curve_store[0].xdata,curve_store[0].ydata);
		}

		std::cin.get();

	}

	void plotlogx(double* x, double* y, unsigned int length){
		default_context.open_gp_pipe();
		*default_context.gpp_stream<<"set logscale x"<<endl;
		plot(x, y,length);
	}

	void plotlogx(){
		default_context.open_gp_pipe();
		*default_context.gpp_stream<<"set logscale x"<<endl;
		plot();
	}

	void plotlogy(double* x, double* y, unsigned int length){
		default_context.open_gp_pipe();
		*default_context.gpp_stream<<"set logscale y"<<endl;
		plot(x, y,length);
	}

	void plotlogy(){
		default_context.open_gp_pipe();
		*default_context.gpp_stream<<"set logscale y"<<endl;
		plot();
	}

	void plotlogxy(double* x, double* y, unsigned int length){
		default_context.open_gp_pipe();
		*default_context.gpp_stream<<"set logscale xy"<<endl;
		plot(x, y,length);
	}

	void plotlogxy(){
		default_context.open_gp_pipe();
		*default_context.gpp_stream<<"set logscale xy"<<endl;
		plot();
	}

}

#endif // GPI_H_INCLUDED
